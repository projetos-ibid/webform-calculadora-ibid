﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// As Informações Gerais sobre um assembly são controladas por meio do 
// conjunto de atributos a seguir. Altere esses valores de atributo para modificar as informações
// associadas a um assembly.
[assembly: AssemblyTitle("WebProject_Calculadora_IBID")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("WebProject_Calculadora_IBID")]
[assembly: AssemblyCopyright("Copyright ©  2023")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Configurar o ComVisible como false torna os tipos desse assembly invisíveis 
// para componentes COM.  Se for necessário acessar um tipo nesse assembly a partir do 
// COM, defina o atributo ComVisible como true nesse tipo.
[assembly: ComVisible(false)]

// A GUID a seguir será referente à ID do typelib se este projeto for exposto ao COM
[assembly: Guid("6cc753d8-ff88-49eb-ad6e-b5d7cb5319a7")]

// As informações de versão de um assembly consistem nos quatro valores a seguir:
//
//      Versão Principal
//      Versão Secundária 
//      Número da Versão
//      Revisão
//
// É possível especificar todos os valores ou utilizar como padrão os Números de Revisão e da Versão 
// usando o '*' como mostrado abaixo:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
