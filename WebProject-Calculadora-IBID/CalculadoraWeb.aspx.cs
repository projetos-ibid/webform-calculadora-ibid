﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebProject_Calculadora_IBID
{
    public partial class CalculadoraWeb : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCalcular_Click(object sender, EventArgs e)
        {
            switch (ddlOperadores.SelectedValue)
            {
                case "0":
                    lblResultadoConta.Text = (Convert.ToDouble(tbValor1.Text) + Convert.ToDouble(tbValor2.Text)).ToString();
                    break;
                case "1":
                    lblResultadoConta.Text = (Convert.ToDouble(tbValor1.Text) - Convert.ToDouble(tbValor2.Text)).ToString();
                    break;
                case "2":
                    if (tbValor1.Text == "0" || tbValor2.Text == "0")
                    {
                        lblResultadoConta.Text = "Não é possivel fazer uma divisão com 0, tente novamente!";
                        return;
                    }
                    else
                    {
                        lblResultadoConta.Text = (Convert.ToDouble(tbValor1.Text) - Convert.ToDouble(tbValor2.Text)).ToString();
                    }
                    break;
                case "3":
                    lblResultadoConta.Text = (Convert.ToDouble(tbValor1.Text) * Convert.ToDouble(tbValor2.Text)).ToString();
                    break;
                default:
                    lblResultadoConta.Text = "Selecione uma operação válida.";
                    return;
            }
        }
    }
}