﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CalculadoraWeb.aspx.cs" Inherits="WebProject_Calculadora_IBID.CalculadoraWeb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Calculadora</title>
    <link rel="stylesheet" type="text/css" href="Assets/css/formStyle.css" />
    <style type="text/css">
        .auto-style1 {
            color: #A33E54;
            font-weight: 500;
            width: 202px;
        }
        .auto-style2 {
            width: 202px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" class="main-div">
        <div id="main">
            <h1>Calculadora ❤</h1>
            <table>
                <tr>
                    <td class="color-font-valor">Valor 1:</td>
                    <td class="auto-style1">Valor 2:</td>
                </tr>
                <tr>
                    <td><asp:TextBox ID="tbValor1" runat="server" class="caixa-valor"></asp:TextBox></td>
                    <td class="auto-style2"><asp:TextBox ID="tbValor2" runat="server" class="caixa-valor"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="color-font">Operadores:</td>
                </tr>
                <tr>
                    <td><asp:DropDownList ID="ddlOperadores" runat="server" Height="38px" Width="165px" class="caixa-valor-dp">
                        <asp:ListItem></asp:ListItem>
                        <asp:ListItem Value="0">+ (somar)</asp:ListItem>
                        <asp:ListItem Value="1">- (subtrair)</asp:ListItem>
                        <asp:ListItem Value="2">/ (dividir)</asp:ListItem>
                        <asp:ListItem Value="3">x (multiplicar)</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="auto-style2"><asp:Button ID="btnCalcular" runat="server" Text="Calcular" OnClick="btnCalcular_Click" class="botao-calcular"/></td>
                </tr>
                <tr>
                    <td class="color-font-resultado"><p>Resultado: </p></td>
                    <td class="auto-style2"><asp:Label ID="lblResultadoConta" runat="server"></asp:Label></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
